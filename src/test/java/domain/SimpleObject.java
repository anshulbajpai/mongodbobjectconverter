package domain;

import com.mongodbobjectcoverter.annotations.DBEnum;
import com.mongodbobjectcoverter.annotations.DBField;
import com.mongodbobjectcoverter.annotations.DBObject;

import java.math.BigDecimal;

@DBObject
public class SimpleObject{

    @DBField
    private String firstName = "Anshul";
    @DBField
    private int age = 10;
    @DBField
    private boolean isStudent = true;
    @DBField
    private BigDecimal bigValue = BigDecimal.TEN;
    @DBEnum
    private MyEnum enums = MyEnum.Some;

    private String name = "some name";

    public void setEnums(MyEnum myEnum) {
        enums = myEnum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleObject that = (SimpleObject) o;

        if (age != that.age) return false;
        if (isStudent != that.isStudent) return false;
        if (bigValue != null ? !bigValue.equals(that.bigValue) : that.bigValue != null) return false;
        if (enums != that.enums) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (isStudent ? 1 : 0);
        result = 31 * result + (bigValue != null ? bigValue.hashCode() : 0);
        result = 31 * result + (enums != null ? enums.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}