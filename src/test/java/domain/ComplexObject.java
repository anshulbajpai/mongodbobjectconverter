package domain;


import com.mongodbobjectcoverter.annotations.DBField;
import com.mongodbobjectcoverter.annotations.DBList;
import com.mongodbobjectcoverter.annotations.DBObject;

import java.util.ArrayList;
import java.util.List;

@DBObject
public class ComplexObject{

    @DBObject
    private SimpleObject simpleObject = new SimpleObject();

    @DBField
    private String lastName = "Bajpai";

    @DBList
    private List<SimpleObject> simpleObjects = new ArrayList<SimpleObject>();

    @DBList
    private List<String> simpleNames = new ArrayList<String>();

    public List<SimpleObject> getSimpleObjects() {
        return simpleObjects;
    }

    public List<String> getSimpleNames() {
        return simpleNames;
    }
}
