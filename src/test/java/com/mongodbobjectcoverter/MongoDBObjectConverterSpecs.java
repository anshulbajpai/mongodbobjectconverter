package com.mongodbobjectcoverter;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import domain.ComplexObject;
import domain.SimpleObject;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;

public class MongoDBObjectConverterSpecs {

    private MongoDBObjectConverter mongoDBObjectConverter;

    @Before
    public void setUp() throws Exception {
        mongoDBObjectConverter = new MongoDBObjectConverter();
    }

    @Test(expected = IllegalArgumentException.class)
    public void itThrowsExceptionIfNotADbObject() {
        mongoDBObjectConverter.convert(new Object());
    }

    @Test
    public void itConvertsASimpleObject() {
        BasicDBObject basicDBObject = mongoDBObjectConverter.convert(new SimpleObject());
        assertThat(basicDBObject.get("firstName")).isEqualTo("Anshul");
        assertThat(basicDBObject.get("age")).isEqualTo(10);
        assertThat(basicDBObject.get("isStudent")).isEqualTo(true);
        assertThat(basicDBObject.get("bigValue")).isEqualTo(BigDecimal.TEN);
    }

    @Test
    public void itConvertsEnum() {
        BasicDBObject basicDBObject = mongoDBObjectConverter.convert(new SimpleObject());
        assertThat(basicDBObject.get("enums")).isEqualTo("Some");
    }

    @Test
    public void itConvertsEnumWithBlankValueIfEnumIsNull() {
        SimpleObject simpleObject = new SimpleObject();
        simpleObject.setEnums(null);
        BasicDBObject basicDBObject = mongoDBObjectConverter.convert(simpleObject);
        assertThat(basicDBObject.get("enums")).isEqualTo("");
    }

    @Test
    public void itOnlyConvertsA_DBField() {
        BasicDBObject basicDBObject = mongoDBObjectConverter.convert(new SimpleObject());
        assertThat(basicDBObject.get("name")).isNull();
    }

    @Test
    public void itConvertsAComplexObject() {

        ComplexObject complexObject = new ComplexObject();
        BasicDBObject complexDBObject = mongoDBObjectConverter.convert(complexObject);

        assertThat(complexDBObject.get("lastName")).isEqualTo("Bajpai");
        BasicDBObject basicDBObject = (BasicDBObject) complexDBObject.get("simpleObject");
        assertThat(basicDBObject.get("firstName")).isEqualTo("Anshul");
        assertThat(basicDBObject.get("age")).isEqualTo(10);
        assertThat(basicDBObject.get("isStudent")).isEqualTo(true);
        assertThat(basicDBObject.get("bigValue")).isEqualTo(BigDecimal.TEN);
    }


    @Test
    public void itConvertsAComplexObjectWithListOfSimpleObjects() {

        ComplexObject complexObject = new ComplexObject();
        complexObject.getSimpleObjects().add(new SimpleObject());
        complexObject.getSimpleObjects().add(new SimpleObject());
        complexObject.getSimpleNames().add("One");
        complexObject.getSimpleNames().add("Two");
        BasicDBObject complexDBObject = mongoDBObjectConverter.convert(complexObject);

        BasicDBList simpleObjects = (BasicDBList) complexDBObject.get("simpleObjects");
        assertThat(simpleObjects.size()).isEqualTo(2);
        BasicDBList simpleNames = (BasicDBList) complexDBObject.get("simpleNames");
        assertThat(simpleNames.size()).isEqualTo(2);
    }

    @Test
    public void itConvertsFromDocumentToSimpleObject(){
        BasicDBObject basicDBObject = mongoDBObjectConverter.convert(new SimpleObject());
        SimpleObject simpleObject = mongoDBObjectConverter.convert(basicDBObject, SimpleObject.class);
        assertThat(simpleObject).isEqualTo(new SimpleObject());
    }
}
