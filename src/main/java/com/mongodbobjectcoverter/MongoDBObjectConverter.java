package com.mongodbobjectcoverter;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodbobjectcoverter.annotations.DBEnum;
import com.mongodbobjectcoverter.annotations.DBField;
import com.mongodbobjectcoverter.annotations.DBList;
import com.mongodbobjectcoverter.annotations.DBObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class MongoDBObjectConverter {

    public BasicDBObject convert(Object object)  {

        Class klass = object.getClass();
        if(klass.isAnnotationPresent(DBObject.class)){
            BasicDBObject basicDBObject = new BasicDBObject();
            for (Field field : Arrays.asList(klass.getDeclaredFields())) {
                field.setAccessible(true);
                Object value = getValue(object, field);
                if(field.isAnnotationPresent(DBField.class)){
                    basicDBObject.put(field.getName(), value);
                }
                if(field.isAnnotationPresent(DBEnum.class)){
                    basicDBObject.put(field.getName(), getEnumName(value));
                }
                if(field.isAnnotationPresent(DBObject.class)){
                    basicDBObject.put(field.getName(), convert(value));
                }
                if(field.isAnnotationPresent(DBList.class)){
                    if(value.getClass().isAnnotationPresent(DBObject.class))
                        basicDBObject.put(field.getName(), createComplexDBListFor(value));
                    else
                        basicDBObject.put(field.getName(), createSimpleDBListFor(value));

                }
            }
            return basicDBObject;
        }
        throw new IllegalArgumentException();
    }

    private Object getValue(Object object, Field field) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Object getEnumName(Object value)  {
        if(value == null)
            return "";
        return ((Enum)value).name();
    }

    private BasicDBList createSimpleDBListFor(Object value)  {
        BasicDBList basicDBList = new BasicDBList();
        for (Object o : (List) value) {
            basicDBList.add(o);
        }
        return basicDBList;
    }

    private BasicDBList createComplexDBListFor(Object value)  {
        BasicDBList basicDBList = new BasicDBList();
        for (Object o : (List) value) {
            basicDBList.add(convert(o));
        }
        return basicDBList;
    }

    public <T> T  convert(BasicDBObject basicDBObject, Class klass) {
        Object object;
        try {
            object = klass.getConstructor().newInstance();
            List<Field> fields = Arrays.asList(klass.getDeclaredFields());
            for (Field field : fields) {
                field.setAccessible(true);
                if(field.isAnnotationPresent(DBField.class)){
                   field.set(object, basicDBObject.get(field.getName()));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException();
        }
       return (T) object;
    }
}
